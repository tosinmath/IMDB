package com.imdb.ui.list

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withClassName
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.test.suitebuilder.annotation.LargeTest
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.imdb.R
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.ExecutionException


@LargeTest
@RunWith(AndroidJUnit4::class)
class MoviesListUITest {

    // This tests checks if the title TextView value (on the Movies List RecyclerView) is Equal to DetailActivity title.
    // This checks the accuracy of data If/When user clicks on an Item on the List

    var recyclerView: RecyclerView? = null

    @Rule @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    @Throws(InterruptedException::class, ExecutionException::class)
    fun moviesListTest() {

        Thread.sleep(4000)

        recyclerView = mActivityTestRule.activity.findViewById(R.id.moviesRecyclerview)
        val viewHolder = recyclerView!!.findViewHolderForAdapterPosition(0)
        val titleOnList = (viewHolder.itemView.findViewById<View>(R.id.title) as TextView).text.toString()

        val view = Espresso.onView(
                Matchers.allOf(ViewMatchers.withId(R.id.moviesRecyclerview),
                        childAtPosition(
                                ViewMatchers.withClassName(Matchers.`is`("android.widget.LinearLayout")),
                                3)))
        view.perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ViewActions.click()))

        Thread.sleep(2000)

        // Perform the check
        Espresso.onView(ViewMatchers.withId(R.id.detailTitle))
                .check(ViewAssertions.matches(hasValueEqualTo(titleOnList)))

    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }

    internal fun hasValueEqualTo(content: String): Matcher<View> {

        return object : TypeSafeMatcher<View>() {

            override fun describeTo(description: Description) {
                description.appendText("Has EditText/TextView the value:  " + content)
            }

            public override fun matchesSafely(view: View?): Boolean {
                if (view !is TextView && view !is EditText) {
                    return false
                }
                if (view != null) {
                    val text: String
                    if (view is TextView) {
                        text = view.text.toString()
                    } else {
                        text = (view as EditText).text.toString()
                    }

                    return text.equals(content, ignoreCase = true)
                }
                return false
            }
        }
    }
}