package com.imdb

import com.imdb.data.ApiService
import com.imdb.data.model.Movies
import com.imdb.data.model.Result
import com.imdb.data.remote.MoviesInteractor
import com.imdb.ui.detail.DetailPresenter
import com.imdb.ui.detail.DetailView
import com.imdb.ui.list.MoviesListPresenter
import com.imdb.ui.list.MoviesListView
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit


class DetailPresenterTest {

    // This tests checks if the Expected methods are called when getMoviesList in DetailPresenter is being initiated.
    // The 2 methods are setMovieDetails(movieDetails: Result?) and showLoading()

    @Mock
    internal var apiService: ApiService? = null

    @Mock
    internal var moviesInteractor: MoviesInteractor? = null

    @Mock
    internal var detailView: DetailView? = null

    private var presenter: DetailPresenter? = null

    var compositeDisposable: CompositeDisposable? = null
    internal var movieId = 269149
    val moviesResult = getResultData()


    companion object {

        @BeforeClass
        fun setUpRxSchedulers() {
            val immediate = object : Scheduler() {
                override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                    return super.scheduleDirect(run, 0, unit)
                }

                override fun createWorker(): Scheduler.Worker {
                    return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
                }
            }

            RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        }
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulers()
        MockitoAnnotations.initMocks(this)

        compositeDisposable = CompositeDisposable()
        Mockito.`when`(moviesInteractor!!.fetchMovieById(apiService!!, movieId)).thenReturn(Observable.just<Result>(moviesResult))

        presenter = DetailPresenter(moviesInteractor!!)
        presenter!!.attachView(detailView!!)
    }

    @Test
    @Throws(Exception::class)
    fun testAPIResponse() {
        presenter!!.getMovieById(apiService!!, compositeDisposable!!, movieId)
        Mockito.verify(detailView)!!.setMovieDetails(moviesResult)
        Mockito.verify(detailView)!!.showLoading()
    }

    private fun getResultData(): Result {

        // Setting up the values for test
        val results = Result();
        results.voteCount = 6477
        results.id = 269149
        results.video = false
        results.voteAverage = 7.7
        results.title = "Zootopia"
        results.popularity = 516.411141
        results.posterPath = "/sM33SANp9z6rXW8Itn7NnG1GOEs.jpg"
        results.overview = "Determined to prove herself, Officer Judy Hopps, the first bunny on Zootopia's police force, jumps at the chance to crack her first case - even if it means partnering with scam-artist fox Nick Wilde to solve the mystery."

        return results;
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        presenter!!.detachView()
    }
}