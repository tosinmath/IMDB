package com.imdb

import android.app.Application
import com.imdb.data.ApiService
import com.imdb.data.model.Movies
import com.imdb.data.model.Result
import com.imdb.data.remote.MoviesInteractor
import com.imdb.ui.list.MoviesListPresenter
import com.imdb.ui.list.MoviesListView
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit


class MoviesListPresenterTest {

    // This tests checks if the Expected methods are called when getMoviesList in MoviesListPresenter is being initiated.
    // The 2 methods are setAdapter(results: List<Result>, loadmore: Boolean) and showLoading()

    @Mock
    internal var apiService: ApiService? = null

    @Mock
    internal var moviesInteractor: MoviesInteractor? = null

    @Mock
    internal var moviesListView: MoviesListView? = null

    private var presenter: MoviesListPresenter? = null

    var compositeDisposable: CompositeDisposable? = null
    internal var pageId = "1"
    val movies = getMoviesData()


    companion object {

        @BeforeClass
        fun setUpRxSchedulers() {
            val immediate = object : Scheduler() {
                override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                    return super.scheduleDirect(run, 0, unit)
                }

                override fun createWorker(): Scheduler.Worker {
                    return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
                }
            }

            RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        }
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulers()
        MockitoAnnotations.initMocks(this)

        compositeDisposable = CompositeDisposable()
        `when`(moviesInteractor!!.fetchMovies(apiService!!, pageId)).thenReturn(Observable.just<Movies>(movies))

        presenter = MoviesListPresenter(moviesInteractor!!)
        presenter!!.attachView(moviesListView!!)
    }

    @Test
    @Throws(Exception::class)
    fun testAPIResponse() {
        presenter!!.getMoviesList(apiService!!, compositeDisposable!!, false, pageId)
        verify(moviesListView)!!.setAdapter(movies.results!!, false)
        verify(moviesListView)!!.showLoading()
    }

    private fun getMoviesData(): Movies {

        // Setting up the values for test
        val movies = Movies()

        movies.page = 1
        movies.totalResults = 1
        movies.totalPages = 1

        val results = Result();
        results.voteCount = 6477
        results.id = 269149
        results.video = false
        results.voteAverage = 7.7
        results.title = "Zootopia"
        results.popularity = 516.411141
        results.posterPath = "/sM33SANp9z6rXW8Itn7NnG1GOEs.jpg"
        results.overview = "Determined to prove herself, Officer Judy Hopps, the first bunny on Zootopia's police force, jumps at the chance to crack her first case - even if it means partnering with scam-artist fox Nick Wilde to solve the mystery."

        val resultsList = mutableListOf<Result>()
        resultsList.add(results)
        movies.results = resultsList

        return movies;
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        presenter!!.detachView()
    }
}