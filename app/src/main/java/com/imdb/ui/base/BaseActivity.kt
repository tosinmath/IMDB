package com.imdb.ui.base

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import com.imdb.R
import com.imdb.di.component.MoviesComponent


abstract class BaseActivity : AppCompatActivity() {

    private val component: MoviesComponent? = null
    private var snackbarOffline: Snackbar? = null

    protected abstract fun setupActivity(component: MoviesComponent?, savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActivity(component, savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    protected abstract fun loadView()

    fun displayOfflineSnackbar(noConnectionStr: String) {
        val snackbarText: TextView
        snackbarOffline = Snackbar.make(findViewById(android.R.id.content), R.string.no_connection_snackbar, Snackbar.LENGTH_INDEFINITE)
        snackbarText = snackbarOffline!!.getView().findViewById(android.support.design.R.id.snackbar_text)
        snackbarText.setTextColor(resources.getColor(R.color.colorPrimary))
        snackbarOffline!!.setAction(R.string.snackbar_action_retry, View.OnClickListener { loadView() })
        snackbarOffline!!.setActionTextColor(resources.getColor(R.color.colorPrimary))
        snackbarOffline!!.show()
    }

    fun hideOfflineSnackBar() {
        if (snackbarOffline != null && snackbarOffline!!.isShown()) {
            snackbarOffline!!.dismiss()
        }
    }

    fun displayFailedConnection() {
        displayOfflineSnackbar(getString(R.string.failed_endpoint))
    }

    fun displayErrorOccurred() {
        displayOfflineSnackbar(getString(R.string.error_occurred))
    }

    fun displayErrorMsg(errorMsg: String) {
        displayOfflineSnackbar(errorMsg)
    }


}