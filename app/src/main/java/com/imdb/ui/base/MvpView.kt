package com.imdb.ui.base


interface MvpView {

    fun showLoading()

    fun hideLoading()

}