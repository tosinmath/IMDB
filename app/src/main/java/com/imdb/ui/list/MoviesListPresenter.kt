package com.imdb.ui.list

import android.app.Application
import com.imdb.data.ApiService
import com.imdb.data.model.Movies
import com.imdb.data.remote.MoviesInteractor
import com.imdb.ui.base.BasePresenter
import com.imdb.util.Logger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException



class MoviesListPresenter(private val moviesInteractor: MoviesInteractor) : BasePresenter<MoviesListView>() {
    private val moviesListView: MoviesListView? = null
    private val logger = Logger.getLogger(javaClass)
    private var loadmore: Boolean = false
    private val jsonErrorStr = "message"

    override fun attachView(moviesListView: MoviesListView) {
        super.attachView(moviesListView)
    }

    override fun detachView() {
        super.detachView()
        mvpView!!.clearDisposable()
    }

    fun getMoviesList(apiService: ApiService, compositeDisposable: CompositeDisposable, loadmore: Boolean, pageId: String) {
        if(isViewAttached) {
            if(!loadmore) mvpView!!.showLoading()
            this.loadmore = loadmore
            compositeDisposable.add(moviesInteractor.fetchMovies(pageId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse, this::handleError));
        }
    }

    fun getMoviesListByDate(apiService: ApiService, compositeDisposable: CompositeDisposable, loadmore: Boolean,
                            pageId: String, primaryReleaseDate: String) {
        if(isViewAttached) {
            if(!loadmore) mvpView!!.showLoading()
            this.loadmore = loadmore
            compositeDisposable.add(moviesInteractor.fetchMoviesByDate(pageId, primaryReleaseDate)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse, this::handleError));
        }
    }

    private fun handleResponse(movies: Movies) {
        mvpView!!.setAdapter(movies.results!!, loadmore)
    }

    private fun handleError(error: Throwable?) {
        if (error is HttpException) {
            val responseBody = (error as HttpException).response().errorBody()
            mvpView!!.displayErrorMsg(getErrorMessage(responseBody!!))
            logger.debug(getErrorMessage(responseBody))
        } else if (error is SocketTimeoutException) {
            mvpView!!.displayFailedConnection();
            logger.debug(error.localizedMessage)
        } else if (error is IOException) {
            mvpView!!.displayErrorOccurred();
            logger.debug(error.localizedMessage)
        } else {
            mvpView!!.displayErrorOccurred()
            logger.debug(error!!.localizedMessage)
        }
    }

    private fun getErrorMessage(responseBody: ResponseBody): String {
        try {
            val jsonObject = JSONObject(responseBody.string())
            return jsonObject.getString(jsonErrorStr)
        } catch (e: Exception) {
            return e.message!!
        }
    }

}