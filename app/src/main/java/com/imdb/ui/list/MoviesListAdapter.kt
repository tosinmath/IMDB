package com.imdb.ui.list

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.imdb.BuildConfig
import com.imdb.R
import com.imdb.data.model.Result
import com.imdb.ui.detail.DetailActivity
import com.imdb.util.ImageUtil
import com.imdb.util.Logger
import com.imdb.util.TextUtil


class MoviesListAdapter(private val context: Context, private val movies: MutableList<Result>?) : RecyclerView.Adapter<MoviesListAdapter.ViewHolder>() {

    private val mTypedValue = TypedValue()
    private val mBackground: Int

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val title: TextView
        val image: ImageView
        val releaseDate: TextView
        val rating: TextView

        init {
            title = mView.findViewById(R.id.title)
            image = mView.findViewById(R.id.image)
            releaseDate = mView.findViewById(R.id.release_date)
            rating = mView.findViewById(R.id.rating)
        }

        override fun toString(): String {
            return super.toString()
        }
    }

    fun getValueAt(position: Int): String {
        return movies!![position].id.toString()
    }

    init {
        context.theme.resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true)
        mBackground = mTypedValue.resourceId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.movies_list, parent, false)
        view.setBackgroundResource(mBackground)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        /* Set your values */
        val model = movies!![position]


        if (model.title != null) holder.title.setText(model.title)
        if (model.voteAverage != null) holder.rating.setText(model.voteAverage.toString())
        if (model.releaseDate != null){
            val releaseDate = TextUtil.formatDate(model.releaseDate!!)
            holder.releaseDate.setText(releaseDate)
        }
        if (model.posterPath != null){
            val imgResource =   BuildConfig.IMAGE_URL + context.getString(R.string.small_image) + model.posterPath.toString()
            ImageUtil.displayImage(holder.image.getContext(),
                    imgResource, holder.image, R.drawable.placeholder_small)
        }

        // launch the detail activity to show movies information
        holder.mView.setOnClickListener { v ->
            val context = v.context
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(context.getString(R.string.movie_id_bundle), movies[holder.adapterPosition].id)
            val activity = v.context as Activity
            activity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return movies?.size ?: 0
    }

    fun addAll(data: List<Result>) {
        movies!!.addAll(data)
        notifyDataSetChanged()
    }

    fun add(data: Result) {
        notifyDataSetChanged()
        movies!!.add(data)
    }

    fun getItemPos(pos: Int): Result {
        return movies!![pos]
    }

    fun clear() {
        movies!!.clear()
    }

}