package com.imdb.ui.list

import android.app.DatePickerDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AbsListView
import android.widget.DatePicker
import com.imdb.BaseApplication
import com.imdb.R
import com.imdb.data.ApiService
import com.imdb.data.model.Movies
import com.imdb.data.model.Result
import com.imdb.di.component.MoviesComponent
import com.imdb.ui.base.BaseActivity
import com.imdb.util.NetworkUtil
import com.imdb.util.TextUtil.Companion.datePadding
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : BaseActivity() , MoviesListView {

    @field:[Inject]
    lateinit var presenter: MoviesListPresenter

    @field:[Inject]
    lateinit var apiService: ApiService

    private var compositeDisposable: CompositeDisposable? = null
    private var layoutManager: LinearLayoutManager? = null
    private var adapter: MoviesListAdapter? = null

    private var userScrolled = true
    private var pastVisiblesItems: Int = 0
    private var visibleItemCount:Int = 0
    private var totalItemCount:Int = 0
    private var page = 1

    lateinit var currentDate: Calendar
    var currentDay: Int = 0
    var currentMonth:Int = 0
    var currentYear:Int = 0

    override fun setupActivity(component: MoviesComponent?, savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_main)
        BaseApplication.moviesComponent.inject(this)
        presenter.attachView(this)
        compositeDisposable = CompositeDisposable()

        initializeViews()
        loadView()
        filterMoviesByDate()
    }

    override fun setAdapter(results: List<Result>, loadmore: Boolean) {
        hideLoading()
        if (results.size > 0) {
            val moviesItemList: MutableList<Result> = ArrayList(results)
            if (!loadmore) {
                adapter = MoviesListAdapter(applicationContext, moviesItemList)
                moviesRecyclerview.setAdapter(adapter)
            } else {
                adapter!!.addAll(moviesItemList)
                hideLoadMore()
            }
        }
    }

    fun initializeViews(){
        layoutManager = LinearLayoutManager(applicationContext)
        moviesRecyclerview.setHasFixedSize(true)
        moviesRecyclerview.setLayoutManager(layoutManager)
    }

    fun implementScrollListener() {
        moviesRecyclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                visibleItemCount = layoutManager!!.getChildCount()
                totalItemCount = layoutManager!!.getItemCount()
                pastVisiblesItems = layoutManager!!.findFirstVisibleItemPosition()

                if (userScrolled && visibleItemCount + pastVisiblesItems == totalItemCount) {
                    userScrolled = false
                    page = page + 1
                    presenter.getMoviesList(apiService, compositeDisposable!!, true, page.toString())
                    showLoadMore()
                }
            }
        })
    }

    override fun loadView(){
        if (NetworkUtil.isConnected(applicationContext)) {
            populateList()
            implementScrollListener()
            hideOfflineSnackBar()
        } else {
            displayOfflineSnackbar(getString(R.string.no_connection_snackbar))
        }
    }

    fun populateList() {
        presenter.getMoviesList(apiService, compositeDisposable!!, false, page.toString())
    }

    fun filterMoviesByDate(){
        currentDate = Calendar.getInstance()
        currentDay = currentDate.get(Calendar.DAY_OF_MONTH)
        currentMonth = currentDate.get(Calendar.MONTH)
        currentYear = currentDate.get(Calendar.YEAR)
        filter.setOnClickListener { v ->
            val datePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val primaryReleaseDate = datePadding(dayOfMonth.toString(), monthOfYear.toString(), year.toString())
                presenter.getMoviesListByDate(apiService, compositeDisposable!!, false, page.toString(), primaryReleaseDate)

            }, currentYear, currentMonth, currentDay)
            datePickerDialog.show()
        }
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    fun showLoadMore() {
        loadMoreItems.visibility = View.VISIBLE
    }

    fun hideLoadMore() {
        loadMoreItems.visibility = View.GONE
    }

    override fun clearDisposable(){
        compositeDisposable!!.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }


}
