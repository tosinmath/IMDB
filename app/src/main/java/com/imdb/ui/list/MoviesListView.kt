package com.imdb.ui.list

import com.imdb.data.model.Result
import com.imdb.ui.base.MvpView


interface MoviesListView : MvpView {

    fun setAdapter(results: List<Result>, loadmore: Boolean)

    override fun showLoading()

    override fun hideLoading()

    fun loadView()

    fun displayFailedConnection();

    fun displayErrorOccurred();

    fun displayErrorMsg(errorMsg: String);

    fun clearDisposable();
}