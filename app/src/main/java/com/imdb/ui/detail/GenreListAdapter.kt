package com.imdb.ui.detail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.imdb.BuildConfig
import com.imdb.R
import com.imdb.data.model.Genre
import com.imdb.data.model.Result
import com.imdb.util.ImageUtil
import com.imdb.util.Logger
import com.imdb.util.TextUtil


class GenreListAdapter(private val context: Context, private val genres: MutableList<Genre>?) : RecyclerView.Adapter<GenreListAdapter.ViewHolder>() {

    private val mTypedValue = TypedValue()
    private val mBackground: Int

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val title: TextView

        init {
            title = mView.findViewById(R.id.title)
        }

        override fun toString(): String {
            return super.toString()
        }
    }

    fun getValueAt(position: Int): String {
        return genres!![position].id.toString()
    }

    init {
        context.theme.resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true)
        mBackground = mTypedValue.resourceId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_genres, parent, false)
        view.setBackgroundResource(mBackground)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        /* Set your values */
        val model = genres!![position]
        if (model.name != null) holder.title.setText(model.name)
    }

    override fun getItemCount(): Int {
        return genres?.size ?: 0
    }
    fun addAll(data: List<Genre>) {
        genres!!.addAll(data)
        notifyDataSetChanged()
    }

    fun add(data: Genre) {
        notifyDataSetChanged()
        genres!!.add(data)
    }

    fun getItemPos(pos: Int): Genre {
        return genres!![pos]
    }

    fun clear() {
        genres!!.clear()
    }

}