package com.imdb.ui.detail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import com.imdb.BaseApplication
import com.imdb.BuildConfig
import com.imdb.R
import com.imdb.data.ApiService
import com.imdb.data.model.Genre
import com.imdb.data.model.Result
import com.imdb.di.component.MoviesComponent
import com.imdb.ui.base.BaseActivity
import com.imdb.ui.list.MoviesListAdapter
import com.imdb.util.ImageUtil
import com.imdb.util.Logger
import com.imdb.util.TextUtil
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_detail.*;
import java.util.ArrayList

class DetailActivity : BaseActivity(), DetailView {

    @field:[Inject]
    lateinit var presenter: DetailPresenter

    @field:[Inject]
    lateinit var apiService: ApiService

    private var toolbar: Toolbar? = null
    private var compositeDisposable: CompositeDisposable? = null
    private var adapter: GenreListAdapter? = null
    private var layoutManager: LinearLayoutManager? = null

    private var movieId: Int = -1
    private val logger = Logger.getLogger(javaClass)

    override fun setupActivity(component: MoviesComponent?, savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_detail)
        BaseApplication.moviesComponent.inject(this)
        presenter.attachView(this)
        compositeDisposable = CompositeDisposable()

        getBundleData()
        setupToolbar()
        loadView()
    }

    fun getBundleData() {
        val extras = intent.extras
        if (extras != null) {
            movieId = extras.getInt(getString(R.string.movie_id_bundle))
        }
    }

    fun setupToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar!!.setTitle("")
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun setMovieDetails(movieDetails: Result?) {

        hideLoading()
        if (movieDetails != null) {
            showLayout()

            // set the movie details
            if (movieDetails.title != null) detailTitle.text = movieDetails.title
            if (movieDetails.posterPath != null){
                val imgResource =   BuildConfig.IMAGE_URL + applicationContext.getString(R.string.small_image) + movieDetails.posterPath.toString()
                ImageUtil.displayImage(image.getContext(),
                        imgResource, image, R.drawable.placeholder_small)
            }
            if (movieDetails.voteAverage != null) rating.text = movieDetails.voteAverage.toString()
            if (movieDetails.releaseDate != null){
                val releDate = TextUtil.formatDate(movieDetails.releaseDate!!)
                releaseDate.setText(releDate)
            }
            if (movieDetails.runtime != null) runtime.text = movieDetails.runtime.toString()
            if (movieDetails.popularity != null) popularity.text = movieDetails.popularity.toString()
            if (movieDetails.overview != null) overview.text = movieDetails.overview


            // set the adapter with list of genres
            displayGenreList(movieDetails.genres!!)


        }
    }

    fun displayGenreList(genres: List<Genre>){
        if (genres.size > 0) {
            val genreItemList: MutableList<Genre> = ArrayList(genres)
            layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
            genreRecyclerview.setHasFixedSize(true)
            genreRecyclerview.setLayoutManager(layoutManager)
            adapter = GenreListAdapter(applicationContext, genreItemList)
            genreRecyclerview.setAdapter(adapter)
            genreRecyclerview.setFocusable(false)
        }
    }

    override fun loadView() {
        hideLayout()
        logger.debug(movieId.toString())
        presenter.getMovieById(apiService, compositeDisposable!!, movieId)
    }

    override fun showLoading() {
        if (progressBar != null && progressBar.visibility == View.GONE) {
            progressBar!!.setVisibility(View.VISIBLE)
        }
    }

    override fun hideLoading() {
        if (progressBar != null && progressBar!!.getVisibility() == View.VISIBLE) {
            progressBar!!.setVisibility(View.GONE)
        }
    }

    private fun hideLayout() {
        detailsContainer.visibility = View.GONE
    }

    private fun showLayout() {
        detailsContainer.visibility = View.VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
        // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable!!.clear()
    }
}
