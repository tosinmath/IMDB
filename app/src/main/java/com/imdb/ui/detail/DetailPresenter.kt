package com.imdb.ui.detail

import com.imdb.data.ApiService
import com.imdb.data.model.Result
import com.imdb.data.remote.MoviesInteractor
import com.imdb.ui.base.BasePresenter
import com.imdb.util.Logger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException


class DetailPresenter(private val moviesInteractor: MoviesInteractor) : BasePresenter<DetailView>() {
    private val detailView: DetailView? = null
    private val logger = Logger.getLogger(javaClass)
    private var loadmore: Boolean = false
    private val jsonErrorStr = "message"

    override fun attachView(detailView: DetailView) {
        super.attachView(detailView)
    }

    override fun detachView() {
        super.detachView()
    }

    fun getMovieById(apiService: ApiService, compositeDisposable: CompositeDisposable, movieId: Int) {
        if(isViewAttached) {
            mvpView!!.showLoading()
            compositeDisposable.add(moviesInteractor.fetchMovieById(movieId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            this::handleResponse,
                            this::handleError
                    ));
        }
    }

    private fun handleResponse(results: Result) {
        mvpView!!.setMovieDetails(results)
    }

    private fun handleError(error: Throwable) {
        if (error is HttpException) {
            val responseBody = (error as HttpException).response().errorBody()
            mvpView!!.displayErrorMsg(getErrorMessage(responseBody!!))
            logger.debug(getErrorMessage(responseBody))
        } else if (error is SocketTimeoutException) {
            mvpView!!.displayFailedConnection();
            logger.debug(error.localizedMessage)
        } else if (error is IOException) {
            mvpView!!.displayErrorOccurred();
            logger.debug(error.localizedMessage)
        } else {
            mvpView!!.displayErrorOccurred()
            logger.debug(error.localizedMessage)
        }
    }

    private fun getErrorMessage(responseBody: ResponseBody): String {
        try {
            val jsonObject = JSONObject(responseBody.string())
            return jsonObject.getString(jsonErrorStr)
        } catch (e: Exception) {
            return e.message!!
        }
    }

}