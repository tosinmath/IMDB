package com.imdb.ui.detail

import com.imdb.data.model.Result
import com.imdb.ui.base.MvpView


interface DetailView : MvpView {

    override fun showLoading()

    override fun hideLoading()

    fun setMovieDetails(movieDetails: Result?)

    fun displayFailedConnection();

    fun displayErrorOccurred();

    fun displayErrorMsg(errorMsg: String);
}