package com.imdb

import android.app.Application
import com.imdb.di.component.DaggerMoviesComponent
import com.imdb.di.component.DaggerNetComponent
import com.imdb.di.component.MoviesComponent
import com.imdb.di.component.NetComponent
import com.imdb.di.module.AppModule
import com.imdb.di.module.MoviesModule
import com.imdb.di.module.NetModule
import com.imdb.di.module.RetrofitModule


class BaseApplication : Application() {
    companion object {
        @JvmStatic lateinit var netComponent: NetComponent
        @JvmStatic lateinit var moviesComponent: MoviesComponent
    }

    override fun onCreate() {
        super.onCreate()

        netComponent = DaggerNetComponent.builder()
                .appModule(AppModule(this))
                .netModule(NetModule())
                .build()

        moviesComponent = DaggerMoviesComponent.builder()
                .netComponent(netComponent)
                .retrofitModule(RetrofitModule())
                .moviesModule(MoviesModule(this))
                .build()
    }
}