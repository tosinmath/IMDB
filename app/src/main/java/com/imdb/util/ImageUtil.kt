package com.imdb.util

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide


class ImageUtil(private val mContext: Context) {
    companion object {

        fun displayImage(context: Context, imageResource: String, imageView: ImageView, drawable : Int) {
            Glide.with(context)
                    .load(imageResource)
                    .placeholder(drawable)
                    .into(imageView)
        }
    }
}