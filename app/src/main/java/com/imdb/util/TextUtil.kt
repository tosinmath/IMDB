package com.imdb.util

import android.text.Html
import android.text.Spanned
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*


class TextUtil() {
    companion object {
        fun formatDate(dateStr: String): String {
            var format = SimpleDateFormat("yyyy-MM-dd")
            var newFormat = SimpleDateFormat("dd-MM-yyyy")
            var date = format.parse(dateStr)
            var str = newFormat.format(date)
            return str
        }

        fun datePadding(day: String, month: String, year: String): String {
            val format = SimpleDateFormat("yyyy-MM-dd")
            val sm = SimpleDateFormat("yyyy-MM-dd")

            val releaseDate = StringBuilder()
            releaseDate.append(year.toString())
            releaseDate.append("-")
            releaseDate.append(month.toString())
            releaseDate.append("-")
            releaseDate.append(day.toString())
            val date = format.parse(releaseDate.toString())
            return sm.format(date);
        }
    }
}