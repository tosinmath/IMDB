package com.imdb.di.component

import com.imdb.ui.list.MainActivity
import com.imdb.di.module.MoviesModule
import com.imdb.di.module.RetrofitModule
import com.imdb.di.scope.UserScope
import com.imdb.ui.detail.DetailActivity
import dagger.Component


@UserScope
@Component(dependencies = arrayOf(NetComponent::class), modules = arrayOf(RetrofitModule::class, MoviesModule::class))
interface MoviesComponent {

    fun inject(mainActivity: MainActivity)
    fun inject(detailActivity: DetailActivity)

}