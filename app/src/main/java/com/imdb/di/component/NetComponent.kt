package com.imdb.di.component

import android.content.SharedPreferences
import com.imdb.di.module.AppModule
import com.imdb.di.module.NetModule
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AppModule::class, NetModule::class))
interface NetComponent {

    // downstream components need these exposed
    fun restAdapter(): Retrofit
    fun sharedPreferences(): SharedPreferences
}