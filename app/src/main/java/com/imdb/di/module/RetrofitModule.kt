package com.imdb.di.module

import com.imdb.data.ApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit


@Module
class RetrofitModule {

    @Provides
    fun providesApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}