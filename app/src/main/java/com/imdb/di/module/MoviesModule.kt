package com.imdb.di.module

import android.app.Application
import com.imdb.data.ApiService
import com.imdb.data.remote.MoviesInteractor
import com.imdb.data.remote.MoviesInteractorImpl
import com.imdb.ui.detail.DetailPresenter
import com.imdb.ui.list.MoviesListPresenter
import dagger.Module
import dagger.Provides


@Module
class MoviesModule(private val application: Application) {

    @Provides
    fun getDetailPresenter(moviesInteractor: MoviesInteractor): DetailPresenter {
        return DetailPresenter(moviesInteractor)
    }

    @Provides
    fun getMoviesListPresenter(moviesInteractor: MoviesInteractor): MoviesListPresenter {
        return MoviesListPresenter(moviesInteractor)
    }

    @Provides
    internal fun provideMoviesFetcher(apiService: ApiService): MoviesInteractor {
        return MoviesInteractorImpl(apiService)
    }


}