package com.imdb.data

import com.imdb.BuildConfig
import com.imdb.data.model.Movies
import com.imdb.data.model.Result
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @GET(BuildConfig.BASE_URL + "discover/movie")
    fun getMovies(@Query("api_key") api_key: String, @Query("page") page: String): Observable<Movies>

    @GET(BuildConfig.BASE_URL + "movie/{movieId}")
    fun getMoviesById(@Path("movieId") movieId: Int, @Query("api_key") api_key: String): Observable<Result>

    @GET(BuildConfig.BASE_URL + "discover/movie")
    fun getMoviesByDate(@Query("api_key") api_key: String, @Query("primary_release_date.gte") primaryReleaseDate: String): Observable<Movies>

}