package com.imdb.data.remote

import android.app.Application
import com.imdb.BuildConfig
import com.imdb.data.ApiService
import com.imdb.data.model.Movies
import com.imdb.data.model.Result
import io.reactivex.Observable

class MoviesInteractorImpl(val apiService: ApiService) : MoviesInteractor {

    override fun fetchMovies(pageId: String):  Observable<Movies> {
        return apiService.getMovies(BuildConfig.API_KEY, pageId)
                .flatMap({ movies -> Observable.just<Movies>(movies) })
                .onErrorReturn { null }
    }

    override fun fetchMovieById(movieId: Int):  Observable<Result> {
        return apiService.getMoviesById(movieId, BuildConfig.API_KEY)
                .flatMap({ result -> Observable.just<Result>(result) })
                .onErrorReturn { null }
    }

    override fun fetchMoviesByDate(pageId: String, primaryReleaseDate: String):  Observable<Movies> {
        return apiService.getMoviesByDate(BuildConfig.API_KEY, primaryReleaseDate)
                .flatMap({ movies -> Observable.just<Movies>(movies) })
                .onErrorReturn { null }
    }
}