package com.imdb.data.remote

import com.imdb.data.ApiService
import com.imdb.data.model.Movies
import com.imdb.data.model.Result
import io.reactivex.Observable


interface MoviesInteractor {
    fun fetchMovies(pageId: String): Observable<Movies>

    fun fetchMovieById(movieId: Int): Observable<Result>

    fun fetchMoviesByDate(pageId: String, primaryReleaseDate: String):  Observable<Movies>
}