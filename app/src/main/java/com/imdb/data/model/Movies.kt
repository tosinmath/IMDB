package com.imdb.data.model


import com.google.gson.annotations.SerializedName
import java.util.HashMap

class Movies {

    var page: Int? = null
    @SerializedName("total_results")
    var totalResults: Int? = null
    @SerializedName("total_pages")
    var totalPages: Int? = null
    var results: List<Result>? = null
}