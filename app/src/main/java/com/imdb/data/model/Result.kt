package com.imdb.data.model


import com.google.gson.annotations.SerializedName
import java.util.HashMap

class Result {

    @SerializedName("vote_count")
    var voteCount: Int? = null
    var id: Int? = null
    var video: Boolean? = null
    @SerializedName("vote_average")
    var voteAverage: Double? = null
    var title: String? = null
    var popularity: Double? = null
    @SerializedName("poster_path")
    var posterPath: String? = null
    @SerializedName("original_language")
    var originalLanguage: String? = null
    @SerializedName("original_title")
    var originalTitle: String? = null
    @SerializedName("genre_ids")
    var genreIds: List<Int>? = null
    @SerializedName("backdrop_path")
    var backdropPath: String? = null
    var adult: Boolean? = null
    var overview: String? = null
    @SerializedName("release_date")
    var releaseDate: String? = null
    @SerializedName("belongs_to_collection")
    var belongsToCollection: Any? = null
    var budget: Int? = null
    var genres: List<Genre>? = null
    var homepage: String? = null
    @SerializedName("imdb_id")
    var imdbId: String? = null
    var revenue: Int? = null
    var runtime: Int? = null
    var status: String? = null
    var tagline: String? = null
}