package com.imdb.data.model


import java.util.HashMap

class Genre {

    var id: Int? = null
    var name: String? = null
}