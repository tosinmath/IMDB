# Android Project
The goal of this project is create a simple app which fetches the data from the API that they provides to see the latest movies, and when the user clicks on any item, see the basic details of the movie. (https://developers.themoviedb.org/3)

## Libraries used

* Google AppCompat Support Libraries
* Google Material Design Support Libraries
* Glide - for image loading
* Rxjava2 - for asynchronous request with observable streams
* Retrofit2 - type-safe REST client to make API request.
* Gson - for parsing the JSON responses
* Dagger2 - for dependency injection
* JUnit - for Unit testing
* Mockito - for creating a "mock" for classes
* Espresso - for User Interface testing


## Building and running the project

* Android Studio 3.0.1
* Android Gradle Plugin 3.0.1

## This App has 2 screens
- First screen with content that is loaded dynamically from https://api.themoviedb.org/3/discover/movie
     *  This first screen contains a RecyclerView that shows content in a list with images, and placeholder for those images.
         - It also contain a Filter Button that shows a DatePicker to filters the movies based on released date.
         - It then filters and only include movies that have a *primary release date* that is greater or equal to the specified date from the DatePicker.

     * Each row in the list is Clickable and has some rating data and released date information.
     * On clicking on an Item, it takes you to Details Activity which In turn shows you all the details regarding the movie including Genres of the movie.

## Other things done.

* The project contain tests Unit Test and UI instrumentation test.
* The design/structure of the application code is production ready and extensible
* This Application is developed entirely in Kotlin
* API 15 as minSdkVersion.
* Fluid layout, that adapts to most phones and tablets.
* All labels, variable names, and identifiers of any kind in English.


## Architecture

I implemented Model View Presenter for this project, also known as MVP, the purpose of this is to break the application into modular, single purpose components.

I decided to choose this architecture because it provides the opportunity to separate concerns and implement "SOLID" principles which makes the application more robust and the code base scalable and maintainable.

## Presenter Layer

I implemented 2 Presenter classes (MoviesListPresenter and DetailPresenter) which sole aim is to be the communicator between the View Layer and Model Layer, acting as a middle man between the two layers.

The 2 Presenters hosts logic to return formatted data from the Model back to the View for display to the user. So each Activity has a matching presenter that handles all access to the model. The presenters passes data to the Activities when the data is ready to display.

## View Layer
The view layer, also known as the UI layer which I named MainActivity and DetailActivity in my assignment, It has a reference to the presenter, which is instantiated in the view. The views have interfaces such as MoviesListView and DetailView, that the presenter interacts with. The main function of the View is to call methods in the Presenter class. The presenters then calls appropriate method in the Views interfaces when data is ready to display.

## Model Layer
The Model Layer, which primarily consist of ( Movies.kt, Result.kt and Genre.kt), They represent the source of the data that I wish to display on the View Layer. It has to do with fetching data from the API, in my case, I implemented **MoviesInteractor** class for this purpose.

The ultimate goal is to expose an interface that defines the different queries that need to be performed in order to abstract away the type of data source used.

In order for presenters to be served specific related data only.

## Interactors

The MovieInteractorImpl will fetch data from my API data source from (https://api.themoviedb.org/3/discover/movie) supplying primary_release_date.gte to filter data. After getting the data, the interactor will send the data to the presenter. Thus, making changes in the UI by calling the appropriate method in the Views interfaces when data is ready to display.

## Testing

* This projects has unit tests using JUnit and mockito for mocking objects.
* This project has User Interface test using Espresso testing framework.


I hope to get an opportunity to talk more about my approach and learn ways to implement these more efficiently. Thank you.

Please, see screenshots below:

![](images/screenshot_1.png)
![](images/screenshot_2.png)
![](images/screenshot_3.png)